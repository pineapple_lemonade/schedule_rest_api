package ru.itis.ruzavin.schedule_rest_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScheduleRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScheduleRestApiApplication.class, args);
	}

}
