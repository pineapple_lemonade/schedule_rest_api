package ru.itis.ruzavin.schedule_rest_api.advisor;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.itis.ruzavin.schedule_rest_api.exceptions.ClientAlreadyAddedException;
import ru.itis.ruzavin.schedule_rest_api.exceptions.ClientNotFoundException;
import ru.itis.ruzavin.schedule_rest_api.exceptions.TimeException;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

	@ExceptionHandler(ClientAlreadyAddedException.class)
	public ResponseEntity<Object> handleClientAlreadyException(ClientAlreadyAddedException exception) {
		Map<String, Object> body = new HashMap<>();
		body.put("timestamp", LocalDateTime.now());
		body.put("message", exception.getMessage());

		return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
				.body(body);
	}

	@ExceptionHandler(ClientNotFoundException.class)
	public ResponseEntity<Object> handleClientNotFound(ClientNotFoundException exception) {
		Map<String, Object> body = new HashMap<>();
		body.put("timestamp", LocalDateTime.now());
		body.put("message", exception.getMessage());

		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(body);
	}

	@ExceptionHandler(TimeException.class)
	public ResponseEntity<Object> handleTimeException(TimeException exception) {
		Map<String, Object> body = new HashMap<>();
		body.put("timestamp", LocalDateTime.now());
		body.put("message", exception.getMessage());

		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(body);
	}
}
