package ru.itis.ruzavin.schedule_rest_api.util;

import ru.itis.ruzavin.schedule_rest_api.dto.MeetingDto;
import ru.itis.ruzavin.schedule_rest_api.dto.time.MeetingDate;
import ru.itis.ruzavin.schedule_rest_api.dto.time.MeetingTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/*
* To simplify my task I decided to work only in one day
* and set that minimum interval is 1 minute
* so all units of time will be minutes
 */
public class TimeUtil {

	public static final Integer MAX_DAY_TIME = 1440;
	public static final Integer MIN_DAY_TIME = 0;

	public static Integer parseString(String date) {
		String[] split = date.split(":");
		int hours = Integer.parseInt(split[0]);
		int minutes = Integer.parseInt(split[1]);

		return hours * 60 + minutes;
	}

	public static String parseInt(Integer date) {
		String hours = String.valueOf(date / 60);

		if (date / 60 == 24 || date / 60 == 0) {
			hours = "00";
		}

		String minutes = String.valueOf(date % 60);

		if (minutes.equals("0")) {
			minutes = "00";
		}

		return hours + ":" + minutes;
	}

	public static boolean isTimeSlotFree(MeetingTime timeSlot, List<MeetingTime> timetableOfClient) {
		for (MeetingTime timeSlotInTimetable : timetableOfClient) {
			if (timeSlot.getStarts() > timeSlotInTimetable.getStarts() &&
					timeSlot.getEnds() < timeSlotInTimetable.getEnds() && timeSlotInTimetable.getIsFree()) {
				return true;
			}
		}
		return false;
	}

	public static List<MeetingTime> getAllFreeIntervals(List<MeetingTime> timetable) {
		return timetable.stream()
				.filter(MeetingTime::getIsFree)
				.filter(meetingTime -> !meetingTime.getStarts().equals(meetingTime.getEnds()))
				.sorted(Comparator.comparingInt(MeetingTime::getStarts))
				.collect(Collectors.toList());
	}

	public static List<MeetingTime> createTimetable(List<MeetingDto> meetings) {
		List<MeetingTime> timetableFirstInstance = meetings.stream()
				.map(meeting -> MeetingTime.getInstance(meeting.getStarts(), meeting.getEnds(), false))
				.sorted(Comparator.comparingInt(MeetingTime::getStarts))
				.collect(Collectors.toList());

		List<MeetingTime> timetable = new ArrayList<>();

		timetable.add(MeetingTime.getInstance(MIN_DAY_TIME, timetableFirstInstance.get(0).getStarts(), true));
		timetable.add(MeetingTime.getInstance(timetableFirstInstance.get(timetableFirstInstance.size() - 1).getEnds(),
				MAX_DAY_TIME
				,true));

		for (int i = 0; i < timetableFirstInstance.size() - 1; i++) {
			if (timetableFirstInstance.get(i).getEnds() >= timetableFirstInstance.get(i + 1).getStarts()) {
				continue;
			}

			timetable.add(MeetingTime.getInstance(timetableFirstInstance.get(i).getEnds(),
					timetableFirstInstance.get(i + 1).getStarts(), true));
			timetable.add(timetableFirstInstance.get(i));
		}

		return timetable.stream()
				.sorted(Comparator.comparingInt(MeetingTime::getStarts))
				.distinct()
				.collect(Collectors.toList());
	}

	public static List<MeetingDate> findSeveralClientsFreeTime(List<List<MeetingTime>> freeTimes) {
		List<MeetingTime> resultList = new ArrayList<>(freeTimes.get(0));

		for (int i = 1; i < freeTimes.size(); i++) {
			findTwoClientsFreeTime(resultList, freeTimes.get(i));
		}
		return resultList.stream()
				.map(meetingTime -> MeetingDate.getInstance(meetingTime.getStarts(), meetingTime.getEnds()))
				.collect(Collectors.toList());
	}

	private static List<MeetingTime> findTwoClientsFreeTime(List<MeetingTime> freeTimes, List<MeetingTime> freeTimes1) {
		List<MeetingTime> timeline = new ArrayList<>(freeTimes);

		timeline.addAll(freeTimes1);
		timeline.sort(Comparator.comparingInt(MeetingTime::getStarts));

		List<MeetingTime> resultList = new ArrayList<>();

		for (int i = 0; i < timeline.size() - 1; i++) {
			Integer starts = timeline.get(i + 1).getStarts();
			Integer ends = timeline.get(i).getEnds();
			if (starts - ends > 0) {
				resultList.add(MeetingTime.getInstance(starts - ends + 1, ends - 1, true));
			}
		}

		return resultList;
	}
}
