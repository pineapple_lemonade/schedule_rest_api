package ru.itis.ruzavin.schedule_rest_api.models;

import lombok.*;

import javax.persistence.*;
import java.util.Calendar;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "meetings")
public class Meeting {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String starts;

	private String ends;

	private String description;

	@ManyToMany(mappedBy = "meetings")
	@ToString.Exclude
	private List<Client> clients;
}
