package ru.itis.ruzavin.schedule_rest_api.services;

import ru.itis.ruzavin.schedule_rest_api.dto.requests.ClientAddingRequest;
import ru.itis.ruzavin.schedule_rest_api.dto.ClientDto;
import ru.itis.ruzavin.schedule_rest_api.dto.requests.SeveralClientsFreeTimeRequest;
import ru.itis.ruzavin.schedule_rest_api.dto.time.MeetingDate;

import java.util.List;
import java.util.Optional;

public interface ClientsService {
	Optional<ClientDto> createClient(ClientAddingRequest client);
	List<MeetingDate> getAllFreeTimeOfClient(String login);
	List<MeetingDate> getAllFreeTimeOfClients(SeveralClientsFreeTimeRequest request);
}
