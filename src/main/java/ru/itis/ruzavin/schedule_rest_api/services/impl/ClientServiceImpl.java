package ru.itis.ruzavin.schedule_rest_api.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.ruzavin.schedule_rest_api.dto.requests.ClientAddingRequest;
import ru.itis.ruzavin.schedule_rest_api.dto.ClientDto;
import ru.itis.ruzavin.schedule_rest_api.dto.MeetingDto;
import ru.itis.ruzavin.schedule_rest_api.dto.requests.SeveralClientsFreeTimeRequest;
import ru.itis.ruzavin.schedule_rest_api.dto.time.MeetingDate;
import ru.itis.ruzavin.schedule_rest_api.exceptions.ClientNotFoundException;
import ru.itis.ruzavin.schedule_rest_api.models.Client;
import ru.itis.ruzavin.schedule_rest_api.repositories.ClientsRepository;
import ru.itis.ruzavin.schedule_rest_api.services.ClientsService;
import ru.itis.ruzavin.schedule_rest_api.dto.time.MeetingTime;
import ru.itis.ruzavin.schedule_rest_api.util.TimeUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientsService {

	private final ClientsRepository clientsRepository;

	@Override
	public Optional<ClientDto> createClient(ClientAddingRequest request) {
		Client client = Client.builder()
				.login(request.getLogin())
				.nickname(request.getNickname())
				.build();

		Optional<Client> clientFromDB = clientsRepository.findByLogin(client.getLogin());
		if (clientFromDB.isPresent()) {
			return Optional.empty();
		} else {
			return Optional.of(ClientDto.from(clientsRepository.save(client))) ;
		}
	}

	@Override
	public List<MeetingDate> getAllFreeTimeOfClient(String login) {
		Optional<Client> clientOptional = clientsRepository.findByLogin(login);

		Client client = clientOptional.orElseThrow(() -> new ClientNotFoundException("Client not found exception"));

		if (client.getMeetings().size() == 0) {
			return List.of(MeetingDate.getInstance(TimeUtil.MIN_DAY_TIME, TimeUtil.MAX_DAY_TIME));
		}

		List<MeetingTime> timetable = TimeUtil.createTimetable(MeetingDto.from(client.getMeetings()));

		List<MeetingTime> allFreeIntervals = TimeUtil.getAllFreeIntervals(timetable);

		return allFreeIntervals.stream()
				.map((meetingTime) -> MeetingDate.getInstance(meetingTime.getStarts(), meetingTime.getEnds()))
				.collect(Collectors.toList());
	}

	@Override
	public List<MeetingDate> getAllFreeTimeOfClients(SeveralClientsFreeTimeRequest request) {
		List<Client> clients = request.getLogins().stream()
				.map(login -> clientsRepository.findByLogin(login)
						.orElseThrow(() -> new ClientNotFoundException("client not found exception")))
				.distinct()
				.collect(Collectors.toList());

		List<List<MeetingTime>> freeTimes = new ArrayList<>();

		for (Client client : clients) {
			freeTimes.add(TimeUtil.getAllFreeIntervals(
					TimeUtil.createTimetable(
							MeetingDto.from(
									client.getMeetings()))));
		}

		return TimeUtil.findSeveralClientsFreeTime(freeTimes);
	}
}
