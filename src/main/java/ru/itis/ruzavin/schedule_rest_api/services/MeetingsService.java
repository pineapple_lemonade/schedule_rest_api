package ru.itis.ruzavin.schedule_rest_api.services;

import ru.itis.ruzavin.schedule_rest_api.dto.requests.MeetingAddingRequest;
import ru.itis.ruzavin.schedule_rest_api.dto.MeetingDto;
import ru.itis.ruzavin.schedule_rest_api.dto.requests.MeetingMultipleAddingRequest;

import java.util.Map;
import java.util.Optional;

public interface MeetingsService {
	Optional<MeetingDto> addMeetingToClient(MeetingAddingRequest request);

	Map<String, Object> addMeetingsToClients(MeetingMultipleAddingRequest request);
}
