package ru.itis.ruzavin.schedule_rest_api.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.itis.ruzavin.schedule_rest_api.dto.ClientDto;
import ru.itis.ruzavin.schedule_rest_api.dto.requests.MeetingAddingRequest;
import ru.itis.ruzavin.schedule_rest_api.dto.MeetingDto;
import ru.itis.ruzavin.schedule_rest_api.dto.requests.MeetingMultipleAddingRequest;
import ru.itis.ruzavin.schedule_rest_api.dto.time.MeetingTime;
import ru.itis.ruzavin.schedule_rest_api.exceptions.ClientNotFoundException;
import ru.itis.ruzavin.schedule_rest_api.exceptions.TimeException;
import ru.itis.ruzavin.schedule_rest_api.models.Client;
import ru.itis.ruzavin.schedule_rest_api.models.Meeting;
import ru.itis.ruzavin.schedule_rest_api.repositories.ClientsRepository;
import ru.itis.ruzavin.schedule_rest_api.repositories.MeetingsRepository;
import ru.itis.ruzavin.schedule_rest_api.services.MeetingsService;
import ru.itis.ruzavin.schedule_rest_api.util.TimeUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class MeetingsServiceImpl implements MeetingsService {

	private final MeetingsRepository meetingsRepository;

	private final ClientsRepository clientsRepository;

	@Override
	public Optional<MeetingDto> addMeetingToClient(MeetingAddingRequest request) {
		Optional<Client> clientOptional = clientsRepository.findByLogin(request.getLogin());

		Client client = clientOptional.orElseThrow(() -> new ClientNotFoundException("Client with login " + request.getLogin()
				+ " not found"));

		Meeting meeting = Meeting.builder()
				.description(request.getDescription())
				.starts(request.getStarts())
				.ends(request.getEnds())
				.clients(List.of(client))
				.build();

		return Optional.of(saveMeeting(client, meeting));
	}

	@Override
	public Map<String, Object> addMeetingsToClients(MeetingMultipleAddingRequest request) {
		Meeting meeting = MeetingDto.to(request.getMeeting());

		List<Client> clients = request.getClientsLogins().stream()
				.map(login -> {
					Optional<Client> clientOptional = clientsRepository.findByLogin(login);
					return clientOptional.orElseThrow(() -> new ClientNotFoundException("client not found"));
				})
				.distinct()
				.collect(Collectors.toList());

		for (Client client:clients) {
			saveMeeting(client, meeting);
		}

		Map<String, Object> response = new HashMap<>();
		response.put("clients", ClientDto.from(clients));
		response.put("meeting", request.getMeeting());

		return response;
	}

	private MeetingDto saveMeeting(Client client, Meeting meeting) {
		if (client.getMeetings().size() == 0) {
			client.getMeetings().add(meeting);
			return MeetingDto.from(meetingsRepository.save(meeting));
		}

		List<MeetingTime> timetable = TimeUtil.createTimetable(MeetingDto.from(client.getMeetings()));

		boolean isFree = TimeUtil.isTimeSlotFree(MeetingTime.getInstance(meeting.getStarts()
						, meeting.getEnds()
						, false)
				, timetable);

		log.warn("the slot's status isFree is {}", isFree);

		if (!isFree) {
			throw new TimeException("this time is already busy");
		}

		client.getMeetings().add(meeting);

		log.warn("Meeting {} will be added", meeting);

		return MeetingDto.from(meetingsRepository.save(meeting));
	}

}
