package ru.itis.ruzavin.schedule_rest_api.dto.time;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.ruzavin.schedule_rest_api.util.TimeUtil;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MeetingTime {
	private Integer starts;
	private Integer ends;
	private Boolean isFree;

	public static MeetingTime getInstance(String starts, String ends, Boolean isFree) {
		return MeetingTime.builder()
				.starts(TimeUtil.parseString(starts))
				.ends(TimeUtil.parseString(ends))
				.isFree(isFree)
				.build();
	}

	public static MeetingTime getInstance(Integer starts, Integer ends, Boolean isFree) {
		return MeetingTime.builder()
				.starts(starts)
				.ends(ends)
				.isFree(isFree)
				.build();
	}
}
