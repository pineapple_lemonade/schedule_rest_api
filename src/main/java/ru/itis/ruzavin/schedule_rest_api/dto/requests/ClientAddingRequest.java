package ru.itis.ruzavin.schedule_rest_api.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientAddingRequest {
	private String login;
	private String nickname;
}
