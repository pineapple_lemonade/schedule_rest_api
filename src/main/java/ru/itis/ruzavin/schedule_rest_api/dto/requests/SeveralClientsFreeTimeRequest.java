package ru.itis.ruzavin.schedule_rest_api.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SeveralClientsFreeTimeRequest {
	private List<String> logins;
}
