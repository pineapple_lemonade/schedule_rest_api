package ru.itis.ruzavin.schedule_rest_api.dto.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.ruzavin.schedule_rest_api.dto.time.MeetingDate;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientsFreeTimeResponse {
	private List<MeetingDate> freeTime;
}
