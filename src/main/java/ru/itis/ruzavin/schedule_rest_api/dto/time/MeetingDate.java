package ru.itis.ruzavin.schedule_rest_api.dto.time;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.ruzavin.schedule_rest_api.util.TimeUtil;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MeetingDate {
	private String starts;
	private String ends;

	public static MeetingDate getInstance(Integer starts, Integer ends) {
		return MeetingDate.builder()
				.starts(TimeUtil.parseInt(starts))
				.ends(TimeUtil.parseInt(ends))
				.build();
	}
}
