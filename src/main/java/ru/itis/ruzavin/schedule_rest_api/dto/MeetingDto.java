package ru.itis.ruzavin.schedule_rest_api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.ruzavin.schedule_rest_api.models.Meeting;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MeetingDto {
	private Long id;

	private String starts;

	private String ends;

	private String description;

	public static MeetingDto from(Meeting meeting) {

		return MeetingDto.builder()
				.id(meeting.getId())
				.description(meeting.getDescription())
				.starts(meeting.getStarts())
				.ends(meeting.getEnds())
				.build();
	}

	public static List<MeetingDto> from(List<Meeting> meetings) {
		return meetings.stream()
				.map(MeetingDto::from)
				.collect(Collectors.toList());
	}

	public static Meeting to(MeetingDto meetingDto) {
		return Meeting.builder()
				.description(meetingDto.getDescription())
				.starts(meetingDto.getStarts())
				.ends(meetingDto.getEnds())
				.build();
	}
}
