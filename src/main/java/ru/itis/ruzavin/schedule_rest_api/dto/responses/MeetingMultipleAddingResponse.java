package ru.itis.ruzavin.schedule_rest_api.dto.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.ruzavin.schedule_rest_api.dto.ClientDto;
import ru.itis.ruzavin.schedule_rest_api.dto.MeetingDto;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MeetingMultipleAddingResponse {
	private String message;
	private MeetingDto meetingDto;
	private List<ClientDto> clients;
}
