package ru.itis.ruzavin.schedule_rest_api.dto.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.ruzavin.schedule_rest_api.dto.ClientDto;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientAddingResponse {
	private String message;
	private ClientDto clientDto;
}
