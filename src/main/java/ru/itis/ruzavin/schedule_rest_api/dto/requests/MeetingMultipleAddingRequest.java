package ru.itis.ruzavin.schedule_rest_api.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.ruzavin.schedule_rest_api.dto.MeetingDto;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MeetingMultipleAddingRequest {
	private MeetingDto meeting;
	private List<String> clientsLogins;
}
