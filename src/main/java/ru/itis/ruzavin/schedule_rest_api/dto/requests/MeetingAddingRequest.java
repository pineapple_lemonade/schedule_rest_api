package ru.itis.ruzavin.schedule_rest_api.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Calendar;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MeetingAddingRequest {
	private String starts;

	private String ends;

	private String description;

	private String login;

}
