package ru.itis.ruzavin.schedule_rest_api.dto.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.ruzavin.schedule_rest_api.dto.MeetingDto;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MeetingAddingResponse {
	private String message;
	private MeetingDto meetingDto;
}
