package ru.itis.ruzavin.schedule_rest_api.dto;

import lombok.*;
import ru.itis.ruzavin.schedule_rest_api.models.Client;
import ru.itis.ruzavin.schedule_rest_api.dto.time.MeetingTime;
import ru.itis.ruzavin.schedule_rest_api.models.Meeting;
import ru.itis.ruzavin.schedule_rest_api.util.TimeUtil;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientDto {
	private Long id;

	private String login;

	private String nickname;

	@ToString.Exclude
	private List<MeetingTime> timetable;

	public static ClientDto from(Client client) {
		if (client.getMeetings() == null) {
			return ClientDto.builder()
					.id(client.getId())
					.login(client.getLogin())
					.nickname(client.getNickname())
					.build();
		}

		return ClientDto.builder()
				.id(client.getId())
				.login(client.getLogin())
				.nickname(client.getNickname())
				.timetable(TimeUtil.createTimetable(MeetingDto.from(client.getMeetings())))
				.build();
	}

	public static List<ClientDto> from(List<Client> clients) {
		return clients.stream()
				.map(ClientDto::from)
				.collect(Collectors.toList());
	}

}
