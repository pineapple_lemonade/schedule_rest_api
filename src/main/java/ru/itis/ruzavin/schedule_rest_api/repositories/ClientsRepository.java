package ru.itis.ruzavin.schedule_rest_api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.ruzavin.schedule_rest_api.models.Client;

import java.util.Optional;

public interface ClientsRepository extends JpaRepository<Client, Long> {
	Optional<Client> findByLogin(String login);
}
