package ru.itis.ruzavin.schedule_rest_api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.ruzavin.schedule_rest_api.models.Meeting;

public interface MeetingsRepository extends JpaRepository<Meeting, Long> {
}
