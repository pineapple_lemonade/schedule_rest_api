package ru.itis.ruzavin.schedule_rest_api.exceptions;

public class ClientAlreadyAddedException extends RuntimeException{
	public ClientAlreadyAddedException(String message) {
		super(message);
	}
}
