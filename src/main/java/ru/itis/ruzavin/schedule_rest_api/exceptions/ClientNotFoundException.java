package ru.itis.ruzavin.schedule_rest_api.exceptions;

public class ClientNotFoundException extends RuntimeException{
	public ClientNotFoundException(String message) {
		super(message);
	}
}
