package ru.itis.ruzavin.schedule_rest_api.exceptions;

public class TimeException extends RuntimeException{
	public TimeException(String message) {
		super(message);
	}
}
