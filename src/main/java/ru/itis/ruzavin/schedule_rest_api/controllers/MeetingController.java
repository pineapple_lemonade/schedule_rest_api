package ru.itis.ruzavin.schedule_rest_api.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.ruzavin.schedule_rest_api.dto.*;
import ru.itis.ruzavin.schedule_rest_api.dto.requests.MeetingAddingRequest;
import ru.itis.ruzavin.schedule_rest_api.dto.requests.MeetingMultipleAddingRequest;
import ru.itis.ruzavin.schedule_rest_api.dto.responses.MeetingAddingResponse;
import ru.itis.ruzavin.schedule_rest_api.dto.responses.MeetingMultipleAddingResponse;
import ru.itis.ruzavin.schedule_rest_api.services.MeetingsService;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/")
@Slf4j
public class MeetingController {
	private final MeetingsService meetingsService;

	@PostMapping("/meetings/client")
	public ResponseEntity<MeetingAddingResponse> addMeetingToClient(@RequestBody MeetingAddingRequest request) {
		log.warn("request {}", request);

		Optional<MeetingDto> meetingDto = meetingsService.addMeetingToClient(request);

		MeetingDto meetingDto1 = meetingDto.orElseThrow(IllegalArgumentException::new);

		MeetingAddingResponse response = MeetingAddingResponse.builder()
				.meetingDto(meetingDto1)
				.message("meeting successfully added")
				.build();

		return ResponseEntity.status(HttpStatus.CREATED)
				.body(response);
	}

	@PostMapping("/meetings/clients")
	public ResponseEntity<MeetingMultipleAddingResponse> addMeetingsToClients(@RequestBody MeetingMultipleAddingRequest request) {
		Map<String, Object> response = meetingsService.addMeetingsToClients(request);

		return ResponseEntity.status(HttpStatus.CREATED)
				.body(MeetingMultipleAddingResponse.builder()
						.clients((List<ClientDto>) response.get("clients"))
						.meetingDto((MeetingDto) response.get("meeting"))
						.message("meeting added to all included clients")
						.build());
	}
}
