package ru.itis.ruzavin.schedule_rest_api.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.ruzavin.schedule_rest_api.dto.requests.ClientAddingRequest;
import ru.itis.ruzavin.schedule_rest_api.dto.requests.SeveralClientsFreeTimeRequest;
import ru.itis.ruzavin.schedule_rest_api.dto.responses.SeveralClientsFreeTimeResponse;
import ru.itis.ruzavin.schedule_rest_api.dto.responses.ClientAddingResponse;
import ru.itis.ruzavin.schedule_rest_api.dto.ClientDto;
import ru.itis.ruzavin.schedule_rest_api.dto.responses.ClientsFreeTimeResponse;
import ru.itis.ruzavin.schedule_rest_api.dto.time.MeetingDate;
import ru.itis.ruzavin.schedule_rest_api.exceptions.ClientAlreadyAddedException;
import ru.itis.ruzavin.schedule_rest_api.exceptions.ClientNotFoundException;
import ru.itis.ruzavin.schedule_rest_api.services.ClientsService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
@RequiredArgsConstructor
@Slf4j
@ControllerAdvice
public class ClientController {

	private final ClientsService clientsService;

	@PostMapping("/clients")
	public ResponseEntity<ClientAddingResponse> createClient(@RequestBody ClientAddingRequest request) {
		log.warn("request {}",request);

		Optional<ClientDto> resultClient = clientsService.createClient(request);

		ClientDto clientDto = resultClient.orElseThrow(() -> new ClientNotFoundException(request.getLogin() + " already added"));

		ClientAddingResponse response = ClientAddingResponse.builder()
				.clientDto(clientDto)
				.message("Client successfully added")
				.build();

		return ResponseEntity.status(HttpStatus.CREATED)
				.body(response);
	}

	@GetMapping("/clients/{login}")
	public ResponseEntity<ClientsFreeTimeResponse> getFreeIntervalsInClientsTimetable(@PathVariable String login) {
		List<MeetingDate> allFreeTimeOfClient = clientsService.getAllFreeTimeOfClient(login);

		return ResponseEntity.ok()
				.body(ClientsFreeTimeResponse.builder()
						.freeTime(allFreeTimeOfClient)
						.build());
	}

	@PostMapping("/clients/freeTime")
	public ResponseEntity<SeveralClientsFreeTimeResponse> getFreeSeveralClientsIntervals(@RequestBody SeveralClientsFreeTimeRequest
	                                                                                     request) {
		List<MeetingDate> allFreeTimeOfClients = clientsService.getAllFreeTimeOfClients(request);

		return ResponseEntity.ok()
				.body(SeveralClientsFreeTimeResponse.builder()
						.freeDates(allFreeTimeOfClients)
						.build());
	}
}
