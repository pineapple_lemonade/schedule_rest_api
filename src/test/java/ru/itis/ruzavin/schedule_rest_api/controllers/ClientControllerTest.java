package ru.itis.ruzavin.schedule_rest_api.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.itis.ruzavin.schedule_rest_api.advisor.ControllerAdvisor;
import ru.itis.ruzavin.schedule_rest_api.dto.ClientDto;
import ru.itis.ruzavin.schedule_rest_api.dto.requests.ClientAddingRequest;
import ru.itis.ruzavin.schedule_rest_api.dto.requests.SeveralClientsFreeTimeRequest;
import ru.itis.ruzavin.schedule_rest_api.dto.responses.ClientsFreeTimeResponse;
import ru.itis.ruzavin.schedule_rest_api.dto.responses.SeveralClientsFreeTimeResponse;
import ru.itis.ruzavin.schedule_rest_api.dto.time.MeetingDate;
import ru.itis.ruzavin.schedule_rest_api.exceptions.ClientNotFoundException;
import ru.itis.ruzavin.schedule_rest_api.models.Client;
import ru.itis.ruzavin.schedule_rest_api.models.Meeting;
import ru.itis.ruzavin.schedule_rest_api.repositories.ClientsRepository;
import ru.itis.ruzavin.schedule_rest_api.services.ClientsService;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ClientController.class)
@RunWith(SpringRunner.class)
public class ClientControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ClientsService clientsService;

	@MockBean
	private ClientsRepository clientsRepository;

	@MockBean
	private ClientController clientController;

	@MockBean
	private ControllerAdvisor controllerAdvisor;

	@Test
	public void testSuccessfullyAddingClient() throws Exception {
		Client client = new Client();
		client.setLogin("test");
		ClientAddingRequest request = ClientAddingRequest.builder()
				.login("test")
				.nickname("test")
				.build();
		given(clientsService.createClient(request)).willReturn(Optional.of(ClientDto.from(client)));

		mockMvc.perform(post("/api/v1/clients")
				.contentType(MediaType.APPLICATION_JSON)
				.content("""
						{
						    "login":"test",
						    "nickname":"test"
						}""")).andExpect(status().isCreated())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.clientDto.login").value("test"));
	}

	@Test
	public void testAddingClientThatAlreadyExists() throws Exception {
		Client client = new Client();
		client.setLogin("test");
		ClientAddingRequest request = ClientAddingRequest.builder()
				.login("test")
				.nickname("test")
				.build();
		given(clientsService.createClient(request)).willReturn(Optional.empty());

		mockMvc.perform(post("/api/v1/clients")
						.contentType(MediaType.APPLICATION_JSON)
						.content("""
						{
						    "login":"test",
						    "nickname":"test"
						}""")).andExpect(status().is4xxClientError())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.message").value("test already added"));
	}

	@Test
	public void testSuccessfullyGetFreeTimeIntervals() throws Exception {
		Client client = new Client();
		client.setLogin("test");
		client.setMeetings(List.of(Meeting.builder()
				.starts("1:00")
				.ends("2:00")
				.description("kinky party")
				.clients(List.of(client))
				.build()));

		given(clientsService.getAllFreeTimeOfClient(client.getLogin())).willReturn(List.of(MeetingDate.getInstance(0, 60),
				MeetingDate.getInstance(120,1440)));

		ClientsFreeTimeResponse response = ClientsFreeTimeResponse.builder()
				.freeTime(List.of(MeetingDate.getInstance(0, 60),
						MeetingDate.getInstance(120,1440)))
				.build();

		given(clientController.getFreeIntervalsInClientsTimetable("test")).willReturn(ResponseEntity.ok(response));

		mockMvc.perform(get("/api/v1/clients/test"))
				.andExpect(status().isOk())
				.andExpect((content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)))
				.andExpect(content().json("""
						{
						    "freeTime": [
						        {
						            "starts": "00:00",
						            "ends": "1:00"
						        },
						        {
						            "starts": "2:00",
						            "ends": "00:00"
						        }
						    ]
						}
						"""));
	}

	@Test
	public void testClientFreeTimeWithoutMeetings() throws Exception {
		Client client = new Client();
		client.setLogin("test");

		ClientsFreeTimeResponse response = ClientsFreeTimeResponse.builder()
				.freeTime(List.of(MeetingDate.getInstance(0, 1440)))
				.build();

		given(clientController.getFreeIntervalsInClientsTimetable("test")).willReturn(ResponseEntity.ok(response));

		mockMvc.perform(get("/api/v1/clients/test"))
				.andExpect(status().isOk())
				.andExpect((content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)))
				.andExpect(content().json("""
						{
						    "freeTime": [
						        {
						            "starts": "00:00",
						            "ends": "00:00"
						        }
						    ]
						}
						"""));
	}

	@Test
	public void testClientNotFoundWhenTryToGetFreeTime() throws Exception {
		Client client = new Client();
		client.setLogin("test");
		client.setMeetings(List.of(Meeting.builder()
				.starts("1:00")
				.ends("2:00")
				.description("kinky party")
				.clients(List.of(client))
				.build()));

		ClientNotFoundException clientNotFoundException = new ClientNotFoundException("Client with login test not found");

		given(clientController.getFreeIntervalsInClientsTimetable(client.getLogin())).willThrow(clientNotFoundException);

		Map<String, Object> body = new HashMap<>();
		body.put("timestamp", LocalDateTime.now());
		body.put("message", clientNotFoundException.getMessage());

		given(controllerAdvisor.handleClientNotFound(clientNotFoundException)).willReturn(ResponseEntity.status(HttpStatus.NOT_FOUND).body(body));

		mockMvc.perform(get("/api/v1/clients/test"))
				.andExpect(status().isNotFound())
				.andExpect((content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)))
				.andExpect(jsonPath("$.message").value("Client with login test not found"));
	}

	@Test
	public void testSuccessfullyGetClientsFreeTime() throws Exception {
		SeveralClientsFreeTimeRequest request = SeveralClientsFreeTimeRequest.builder()
				.logins(List.of("test","test1"))
				.build();

		SeveralClientsFreeTimeResponse response = SeveralClientsFreeTimeResponse.builder()
				.freeDates(List.of(MeetingDate.getInstance(60, 120)))
				.build();

		given(clientController.getFreeSeveralClientsIntervals(request)).willReturn(ResponseEntity.ok(response));

		mockMvc.perform(post("/api/v1/clients/freeTime")
				.content("""
						{
						        "logins":["test","test1"]
						  }""")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect((content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)))
				.andExpect(content().json("""
						{
						    "freeDates": [
						        {
						            "starts": "1:00",
						            "ends": "2:00"
						        }
						    ]
						}
						"""));
	}

	@Test
	public void testClientOrClientsNotFoundWhenTryToGetFreeTime() throws Exception {
		SeveralClientsFreeTimeRequest request = SeveralClientsFreeTimeRequest.builder()
				.logins(List.of("test","test1"))
				.build();

		ClientNotFoundException clientNotFoundException = new ClientNotFoundException("Client with login test1 not found");

		given(clientController.getFreeSeveralClientsIntervals(request)).willThrow(clientNotFoundException);

		Map<String, Object> body = new HashMap<>();
		body.put("timestamp", LocalDateTime.now());
		body.put("message", clientNotFoundException.getMessage());

		given(controllerAdvisor.handleClientNotFound(clientNotFoundException)).willReturn(ResponseEntity.status(HttpStatus.NOT_FOUND).body(body));

		mockMvc.perform(post("/api/v1/clients/freeTime")
						.content("""
						{
						        "logins":["test","test1"]
						  }""")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound())
				.andExpect((content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)))
				.andExpect(jsonPath("$.message").value("Client with login test1 not found"));
	}

	@Test
	public void testDuplicateClientsGetSameFreeTimeLikeTheyNotDuplicate() throws Exception {
		SeveralClientsFreeTimeRequest request = SeveralClientsFreeTimeRequest.builder()
				.logins(List.of("test","test"))
				.build();

		SeveralClientsFreeTimeResponse response = SeveralClientsFreeTimeResponse.builder()
				.freeDates(List.of(MeetingDate.getInstance(60, 120)))
				.build();

		given(clientController.getFreeSeveralClientsIntervals(request)).willReturn(ResponseEntity.ok(response));

		mockMvc.perform(post("/api/v1/clients/freeTime")
						.content("""
						{
						        "logins":["test","test"]
						  }""")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect((content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)))
				.andExpect(content().json("""
						{
						    "freeDates": [
						        {
						            "starts": "1:00",
						            "ends": "2:00"
						        }
						    ]
						}
						"""));
	}
}
