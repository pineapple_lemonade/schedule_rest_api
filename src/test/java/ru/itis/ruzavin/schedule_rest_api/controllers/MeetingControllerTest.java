package ru.itis.ruzavin.schedule_rest_api.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.itis.ruzavin.schedule_rest_api.advisor.ControllerAdvisor;
import ru.itis.ruzavin.schedule_rest_api.dto.ClientDto;
import ru.itis.ruzavin.schedule_rest_api.dto.MeetingDto;
import ru.itis.ruzavin.schedule_rest_api.dto.requests.MeetingAddingRequest;
import ru.itis.ruzavin.schedule_rest_api.dto.requests.MeetingMultipleAddingRequest;
import ru.itis.ruzavin.schedule_rest_api.dto.responses.MeetingAddingResponse;
import ru.itis.ruzavin.schedule_rest_api.dto.responses.MeetingMultipleAddingResponse;
import ru.itis.ruzavin.schedule_rest_api.exceptions.ClientNotFoundException;
import ru.itis.ruzavin.schedule_rest_api.exceptions.TimeException;
import ru.itis.ruzavin.schedule_rest_api.services.ClientsService;
import ru.itis.ruzavin.schedule_rest_api.services.MeetingsService;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.BDDMockito.given;

@WebMvcTest(ClientController.class)
@RunWith(SpringRunner.class)
public class MeetingControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ControllerAdvisor controllerAdvisor;

	@MockBean
	private MeetingController meetingController;

	@MockBean
	private MeetingsService meetingsService;

	@MockBean
	private ClientsService clientsService;

	@Test
	public void testSuccessfullyAddingMeetingToClient() throws Exception {
		MeetingAddingRequest request = MeetingAddingRequest.builder()
				.login("test")
				.description("kind party")
				.starts("1:00")
				.ends("2:00")
				.build();

		MeetingAddingResponse response = MeetingAddingResponse.builder()
				.message("meeting successfully added")
				.meetingDto(MeetingDto.builder()
						.id(1L)
						.description("kind party")
						.starts("1:00")
						.ends("2:00")
						.build())
				.build();

		given(meetingController.addMeetingToClient(request)).willReturn(ResponseEntity.status(HttpStatus.CREATED).body(response));

		mockMvc.perform(post("/api/v1/meetings/client")
				.contentType(MediaType.APPLICATION_JSON)
				.content("""
						{
						    "description":"kind party",
						    "starts":"1:00",
						    "ends":"2:00",
						    "login":"test"
						}"""))
				.andExpect(status().isCreated())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.message").value("meeting successfully added"));
	}

	@Test
	public void testClientNotFoundWhenAddingMeeting() throws Exception {
		MeetingAddingRequest request = MeetingAddingRequest.builder()
				.login("test")
				.description("kind party")
				.starts("1:00")
				.ends("2:00")
				.build();

		ClientNotFoundException exception = new ClientNotFoundException("Client with login test not found");

		given(meetingController.addMeetingToClient(request)).willThrow(exception);

		Map<String, Object> body = new HashMap<>();
		body.put("timestamp", LocalDateTime.now());
		body.put("message", exception.getMessage());

		given(controllerAdvisor.handleClientNotFound(exception)).willReturn(ResponseEntity.status(HttpStatus.NOT_FOUND).body(body));

		mockMvc.perform(post("/api/v1/meetings/client")
						.contentType(MediaType.APPLICATION_JSON)
						.content("""
						{
						    "description":"kind party",
						    "starts":"1:00",
						    "ends":"2:00",
						    "login":"test"
						}"""))
				.andExpect(status().isNotFound())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.message").value("Client with login test not found"));
	}

	@Test
	public void testTryToAddMeetingOnTimeThatAlreadyBusy() throws Exception {
		MeetingAddingRequest request = MeetingAddingRequest.builder()
				.login("test")
				.description("kind party")
				.starts("1:00")
				.ends("2:00")
				.build();

		TimeException exception = new TimeException("this time is already busy");

		given(meetingController.addMeetingToClient(request)).willThrow(exception);

		Map<String, Object> body = new HashMap<>();
		body.put("timestamp", LocalDateTime.now());
		body.put("message", exception.getMessage());

		given(controllerAdvisor.handleTimeException(exception)).willReturn(ResponseEntity.status(HttpStatus.NOT_FOUND).body(body));


		mockMvc.perform(post("/api/v1/meetings/client")
						.contentType(MediaType.APPLICATION_JSON)
						.content("""
						{
						    "description":"kind party",
						    "starts":"1:00",
						    "ends":"2:00",
						    "login":"test"
						}"""))
				.andExpect(status().isNotFound())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.message").value("this time is already busy"));
	}

	@Test
	public void testSuccessfullyAddingMeetingsToSeveralClients() throws Exception {
		MeetingDto evilParty = MeetingDto.builder()
				.id(1L)
				.starts("1:00")
				.ends("2:00")
				.description("evil party")
				.build();

		MeetingMultipleAddingRequest request = MeetingMultipleAddingRequest.builder()
				.meeting(evilParty)
				.clientsLogins(List.of("test","test1"))
				.build();

		MeetingMultipleAddingResponse response = MeetingMultipleAddingResponse.builder()
				.clients(List.of(ClientDto.builder()
								.login("test")
								.nickname("test")
								.id(1L)
						.build(), ClientDto.builder()
								.id(2L)
								.nickname("test1")
								.login("test1")
						.build()))
				.message("meeting added to all included clients")
				.meetingDto(evilParty)
				.build();

		given(meetingController.addMeetingsToClients(request)).willReturn(ResponseEntity.status(HttpStatus.CREATED).body(response));

		mockMvc.perform(post("/api/v1/meetings/clients")
						.contentType(MediaType.APPLICATION_JSON)
						.content("""
								{
								    "meeting":{
								        "starts": "1:00",
								        "ends": "2:00",
								        "description": "evil party"
								    },
								    "clientsLogins":["test","test1"]
								}"""))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.message").value("meeting added to all included clients"));
	}

	@Test
	public void testClientNotFoundWhenTryingToAddMeetingToSeveralClients() throws Exception {
		MeetingDto evilParty = MeetingDto.builder()
				.id(1L)
				.starts("1:00")
				.ends("2:00")
				.description("evil party")
				.build();

		MeetingMultipleAddingRequest request = MeetingMultipleAddingRequest.builder()
				.meeting(evilParty)
				.clientsLogins(List.of("test","test1"))
				.build();

		ClientNotFoundException exception = new ClientNotFoundException("Client with login test not found");

		given(meetingController.addMeetingsToClients(request)).willThrow(exception);

		Map<String, Object> body = new HashMap<>();
		body.put("timestamp", LocalDateTime.now());
		body.put("message", exception.getMessage());

		given(controllerAdvisor.handleClientNotFound(exception)).willReturn(ResponseEntity.status(HttpStatus.NOT_FOUND).body(body));

		mockMvc.perform(post("/api/v1/meetings/clients")
						.contentType(MediaType.APPLICATION_JSON)
						.content("""
								{
								    "meeting":{
								        "starts": "1:00",
								        "ends": "2:00",
								        "description": "evil party"
								    },
								    "clientsLogins":["test","test1"]
								}"""))
				.andExpect(status().isNotFound())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.message").value("Client with login test not found"));
	}

	@Test
	public void testTryToAddMeetingAtTimeThatAlreadyBusy() throws Exception {
		MeetingDto evilParty = MeetingDto.builder()
				.id(1L)
				.starts("1:00")
				.ends("2:00")
				.description("evil party")
				.build();

		MeetingMultipleAddingRequest request = MeetingMultipleAddingRequest.builder()
				.meeting(evilParty)
				.clientsLogins(List.of("test","test1"))
				.build();

		TimeException exception = new TimeException("this time is already busy");

		given(meetingController.addMeetingsToClients(request)).willThrow(exception);

		Map<String, Object> body = new HashMap<>();
		body.put("timestamp", LocalDateTime.now());
		body.put("message", exception.getMessage());

		given(controllerAdvisor.handleTimeException(exception)).willReturn(ResponseEntity.status(HttpStatus.NOT_FOUND).body(body));


		mockMvc.perform(post("/api/v1/meetings/clients")
						.contentType(MediaType.APPLICATION_JSON)
						.content("""
								{
								    "meeting":{
								        "starts": "1:00",
								        "ends": "2:00",
								        "description": "evil party"
								    },
								    "clientsLogins":["test","test1"]
								}"""))
				.andExpect(status().isNotFound())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.message").value("this time is already busy"));
	}
}
