How api behaviors example(some responses can be different because of data in DB)


_localhost/api/v1/clients_

request
```
    {
    "login":"gosha1",
    "nickname":"gosha1"
    }
```
response
```
{
    "message": "Client successfully added",
    "clientDto": {
        "id": 1,
        "login": "gosha1",
        "nickname": "gosha1",
        "timetable": null
    }
}
```

request again
```
    {
    "login":"gosha1",
    "nickname":"gosha1"
    }
```
response
```
{
    "message": "Client already added",
    "timestamp": "2022-03-30T16:16:14.2207105"
}
```

_localhost/api/v1/meetings/client_

request
```
{
    "description":"bdsm party",
    "starts":"14:30",
    "ends":"15:00",
    "login":"gosha1"
}
```

response

```
{
    "message": "meeting successfully added",
    "meetingDto": {
        "id": 18,
        "starts": "14:30",
        "ends": "15:00",
        "description": "bdsm party"
    }
}
```

request
```
{
    "description":"bdsm party",
    "starts":"14:30",
    "ends":"15:00",
    "login":"gosha3"
}
```

response

```
{
    "message": "Client with login gosha3 not found",
    "timestamp": "2022-03-30T20:05:34.5259501"
}
```

request

```
{
    "description":"bdsm party",
    "starts":"14:30",
    "ends":"15:00",
    "login":"gosha1"
}
```

response

```
{
    "message": "this time is already busy",
    "timestamp": "2022-03-30T20:06:59.4449841"
}
```

_localhost/api/v1/clients/gosha1_

request

response

```
{
    "freeTime": [
        {
            "starts": "00:00",
            "ends": "13:00"
        },
        {
            "starts": "14:00",
            "ends": "14:30"
        },
        {
            "starts": "15:00",
            "ends": "18:30"
        },
        {
            "starts": "19:00",
            "ends": "00:00"
        }
    ]
}
```

_localhost/api/v1/meetings/clients_

request

```
{
    "meeting":{
        "starts": "18:30",
        "ends": "19:00",
        "description": "bdsm party"
    },
    "clientsLogins":["gosha","gosha1"]
}
```

response

```
{
    "message": "this time is already busy",
    "timestamp": "2022-03-30T20:08:59.387834"
}
```

request

```
{
    "meeting":{
        "starts": "18:30",
        "ends": "19:00",
        "description": "bdsm party"
    },
    "clientsLogins":["gosha","gosha1"]
}
```

response

```
{
    "message": "meeting added to all included clients",
    "meetingDto": {
        "id": null,
        "starts": "9:30",
        "ends": "10:00",
        "description": "bdsm party"
    },
    "clients": [
        {
            "id": 2,
            "login": "gosha",
            "nickname": "gosha1",
            "timetable": [
                {
                    "starts": 0,
                    "ends": 570,
                    "isFree": true
                },
                {
                    "starts": 570,
                    "ends": 600,
                    "isFree": false
                },
                {
                    "starts": 600,
                    "ends": 1110,
                    "isFree": true
                },
                {
                    "starts": 1140,
                    "ends": 1440,
                    "isFree": true
                }
            ]
        },
        {
            "id": 1,
            "login": "gosha1",
            "nickname": "gosha1",
            "timetable": [
                {
                    "starts": 0,
                    "ends": 570,
                    "isFree": true
                },
                {
                    "starts": 570,
                    "ends": 600,
                    "isFree": false
                },
                {
                    "starts": 600,
                    "ends": 780,
                    "isFree": true
                },
                {
                    "starts": 780,
                    "ends": 840,
                    "isFree": false
                },
                {
                    "starts": 840,
                    "ends": 870,
                    "isFree": true
                },
                {
                    "starts": 870,
                    "ends": 900,
                    "isFree": false
                },
                {
                    "starts": 900,
                    "ends": 1110,
                    "isFree": true
                },
                {
                    "starts": 1140,
                    "ends": 1440,
                    "isFree": true
                }
            ]
        }
    ]
}
```

request

```
{
    "meeting":{
        "starts": "9:30",
        "ends": "10:00",
        "description": "bdsm party"
    },
    "clientsLogins":["gosha","gosha3"]
}
```

response

```
{
    "message": "client not found",
    "timestamp": "2022-03-30T20:09:49.8228631"
}
```

_localhost/api/v1/clients/freeTime_

request

```
  {
        "logins":["gosha","gosha1"]
  }
```

response

```
{
    "freeDates": [
        {
            "starts": "00:00",
            "ends": "9:30"
        },
        {
            "starts": "10:00",
            "ends": "18:30"
        },
        {
            "starts": "19:00",
            "ends": "00:00"
        }
    ]
}
```
